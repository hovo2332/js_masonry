window.addEventListener('resize', function () {
  if (Masonry.prototype.resize) {
    Masonry.render()
  }
})

function Masonry(selector, size) {
  Masonry.prototype.selector = selector
  Masonry.prototype.width = size.width
  Masonry.prototype.resize = size.resize

  Masonry.render = function () {
    const images = Array.from(document.querySelectorAll(`${Masonry.prototype.selector} li`));
    let images__height = [];
    let widths__sum = 0;
    let min__height = 0;
    let image__margin = 0;
    let min__height__index = 0;

    images.map((img) => {
      img.style.width = `${Masonry.prototype.width}px`;
      widths__sum += Masonry.prototype.width;
      if (widths__sum <= window.innerWidth) {
        images__height.push(img.offsetHeight);
        img.style.cssText = `width:${Masonry.prototype.width}px; left:${widths__sum-Masonry.prototype.width}px`;
        return
      }
      min__height = Math.min.apply(null, images__height);
      min__height__index = images__height.indexOf(min__height);
      image__margin = images[min__height__index].style.left;

      img.style.cssText =
        `
        top:${min__height}px; 
        left:${image__margin};
        width:${Masonry.prototype.width}px
    
      `
      images__height[min__height__index] += img.offsetHeight;
    })
  }
}